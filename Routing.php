<?php
require_once('controller/DefaultController.php');

class Routing
{

    public $routes = [];

    public function _construct()
    {
        $this->routes = 
        [
            'index' => [   'controller' => 'DefaultController',   'action' => 'index' ],
            'login' => [   'controller' => 'DefaultController',   'action' => 'login' ],
            'upload' => [   'controller' => 'UploadController',   'action' => 'upload' ]
        ];
    }
    public function run()
    {
        
       //$page = isset($_GET['page'] )
      // && isset($this->routes[$_GET['page']]) && $_GET['page'] ? $_GET['page'] : 'index';
      $page = isset($_GET['page'])
      && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'index';
      
       if($this->routes [$page])
       {
           $controller =$this->routes[$page]['controller'];
           $action =$this->routes[$page]['action'];
        
           $object = new $controller;
           $object ->$action();


       }
    }
}