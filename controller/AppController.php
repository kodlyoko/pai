<?php

class AppController
{
    private $request=null;
    public function _construct()
    {
        $this->request = strtolower($_SERVER['REQUEST_METHOD']); 
    }

    public function isGet()
    {
        return $this->request === 'get';
    }

    public function isPost()
    {
        return $this->request === 'post';
    }

    public function render(string $fileName = null, $variables = [])//nazwa pliku z katalogu view
    {
        $path= $fileName 
        ? dirname(_DIR_).'\views\\'.get_class($this).'\\'.$fileName. '.php': '';

        $output = 'there isn\'t such file to render';

        if(file_exist($path)){

            extract($variables);

            ob_start();
            include $path;
            $output =ob_get_clean();
        }

        print $output;

    }
}
?>