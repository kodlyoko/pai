<?php
require_once("AppController.php");//

class DefaultController extends AppController
{
    public function _construct()
    {
        parent::_construct();

    }

    public function index()
    {
        
        $this->render('index');

    }
     
    public function login()
    {
        $this->render('login');

    }
}